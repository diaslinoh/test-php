project = "example-php"

app "example-php" {
  labels = {
    "service" = "example-php",
    "env"     = "dev"
  }

  build {
    use "docker-pull" {
        image = "nginx"
        tag = "alpine"

    }
  }

  deploy {
    use "docker" {
        service_port=80
    }
  }
}